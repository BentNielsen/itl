﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITL.Repository
{
    public class UnitOfWork
    {
        private ItlEntities context = new ItlEntities();

        public List<Person> PersonsGet()
        {
            return context.Persons.ToList();
        }
        public Person PersonGet(int id)
        {
            return context.Persons.Find(id);
        }
        public Person PersonCreate(Person p)
        {
            context.Persons.Add(p);
            context.SaveChanges();
            // Added an id
            return p;
        }
        public Person PersonUpdate(Person p)
        {
            var person = context.Persons.Find(p.id);
            person.Name = p.Name;
            context.SaveChanges();
            return p;
        }
        public void PersonDelete(int id)
        {
            Person p = context.Persons.Find(id);
            context.Persons.Remove(p);
            context.SaveChanges();
        }
    }
}
