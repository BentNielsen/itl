
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 03/30/2017 10:20:56
-- Generated from EDMX file: C:\itl\ITL_TEST\ITL.Repository\EfModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ITL.Employees];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Persons]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Persons];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Persons'
CREATE TABLE [dbo].[Persons] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Name] nchar(50)  NOT NULL,
    [CPR] nvarchar(10)  NOT NULL,
    [Roadname] nvarchar(50)  NOT NULL,
    [PostalCode] nvarchar(20)  NOT NULL,
    [City] nvarchar(20)  NOT NULL,
    [HouseNumber] nvarchar(10)  NOT NULL,
    [Email] varchar(100)  NOT NULL,
    [PhoneNumber] nvarchar(20)  NOT NULL,
    [Age] smallint  NOT NULL,
    [CreateDate] time  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'Persons'
ALTER TABLE [dbo].[Persons]
ADD CONSTRAINT [PK_Persons]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------