﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITL.WebUi
{
    public class WebUiManager
    {
        ITL.Repository.UnitOfWork repos = new ITL.Repository.UnitOfWork();

        public PersonVM PersonGet(int id)
        {
            return repos.PersonGet(id).ToView();
        }

        public List<PersonVM> Persons()
        {
            return repos.PersonsGet().ToView().ToList();
        }
        public PersonVM PersonUpdate(PersonVM pvm)
        {

            return repos.PersonUpdate()
        }
    }
}