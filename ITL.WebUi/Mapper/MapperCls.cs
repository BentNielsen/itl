﻿using ITL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ITL.WebUi
{
    public static class MapperCls
    {
        public static PersonVM ToView(this Person p)
        {
            return new PersonVM() {  id = p.id,Name = p.Navn};
        }

        public static IEnumerable<PersonVM> ToView(this IEnumerable<Person> ps)
        {
          foreach (var p in ps) yield return p.ToView();
        }


    }
}