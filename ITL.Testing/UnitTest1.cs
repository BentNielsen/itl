﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ITL.Testing
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            ITL.Repository.UnitOfWork uow = new Repository.UnitOfWork();
            var p = uow.PersonCreate(new Repository.Person() { Name = "ITL", Age=40,City = "Horsens", CPR="2706761235", CreateDate =  TimeSpan.FromMilliseconds(3), Email =  "theodor@itl.dk", HouseNumber="42-44", PhoneNumber = "+4575602997", PostalCode="7500", Roadname= "Nørregade" });
            var np = uow.PersonGet(p.id);
            uow.PersonDelete(p.id);
        }
    }
}
